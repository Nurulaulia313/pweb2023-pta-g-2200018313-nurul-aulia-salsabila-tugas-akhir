<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link type="icon" rel="icon" href="img/icon/favicon.ico" />
    <title>TUGAS AKHIR</title>
    <link rel="stylesheet" href="style.css" />
</head>

<body>
    <!-- Konten lainnya... -->

    <!-- Buku Tamu -->
    <section id="guestbook" class="guestbook">
        <div class="title">
            <h2 class="section-title">Buku Tamu</h2>
            <hr style="width: 500px; height: 5px; border-radius: 10px; color: aliceblue" />
        </div>

        <div class="content">
            <?php
            // Fungsi untuk membaca isi buku tamu dari file
            function readGuestbook()
            {
                $file = "bukutamu.txt";
                if (file_exists($file)) {
                    $content = file_get_contents($file);
                    echo nl2br($content);
                } else {
                    echo "Buku tamu kosong.";
                }
            }

            // Cek apakah ada data yang dikirim dari form
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $nama = $_POST['nama'];
                $pesan = $_POST['pesan'];

                // Simpan data ke file txt
                $file = fopen("bukutamu.txt", "a");
                fwrite($file, "Nama: " . $nama . "\n");
                fwrite($file, "Pesan: " . $pesan . "\n");
                fwrite($file, "====================================\n");
                fclose($file);
            }
            ?>

            <!-- Tampilkan isi buku tamu -->
            <div class="guestbook-entries">
                <?php readGuestbook(); ?>
            </div>

            <!-- Form buku tamu -->
            <form action="guestbook.php" method="post">
                <label for="nama">Nama:</label>
                <input type="text" id="nama" name="nama" required>

                <label for="pesan">Pesan:</label>
                <textarea id="pesan" name="pesan" required></textarea>

                <input type="submit" value="Kirim Pesan">
            </form>
        </div>
    </section>

    <footer class="footer">
        <?php
        $tahun = date("Y");
        echo "© $tahun - Nurul Aulia Salsabila";
        ?>
    </footer>
    <script src="script.js"></script>
</body>

</html>